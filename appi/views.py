from django.shortcuts import render
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exemptZ
# Create your views here.
@csrf_exempt
def home(request):
    import tensorflow as tf
    import os
    import numpy as np
    import os,glob
    from PIL import Image
    import json
    import sys,argparse
    import base64
    import io
    base64_string = request.POST['image']
    BASE_DIR = os.getcwd()
    PATH_TO_CKPT = os.path.join(BASE_DIR, "appi")+'\\static\\graph.pb'
    PATH_TO_IMAGE = os.path.join(BASE_DIR, "appi")+'\\static\\snake.pg'
    #PATH_TO_IMAGE = os.path.join(PATH_TO_IMAGE, "snake.jpg")
    NUM_CLASSES = 2
    detection_graph = tf.Graph()
    with detection_graph.as_default():
        od_graph_def = tf.GraphDef()
        with tf.gfile.GFile(PATH_TO_CKPT, 'rb') as fid:
            serialized_graph = fid.read()
            od_graph_def.ParseFromString(serialized_graph)
            tf.import_graph_def(od_graph_def, name='')

        sess = tf.Session(graph=detection_graph)

    image_tensor = detection_graph.get_tensor_by_name('image_tensor:0')
    detection_boxes = detection_graph.get_tensor_by_name('detection_boxes:0')
    detection_scores = detection_graph.get_tensor_by_name('detection_scores:0')
    detection_classes = detection_graph.get_tensor_by_name('detection_classes:0')
    num_detections = detection_graph.get_tensor_by_name('num_detections:0')
    #imgdata = base64.b64decode(str(base64_string))
    image = Image.open(io.BytesIO(base64.b64decode(base64_string)))
    #image = Image.open( PATH_TO_IMAGE )
    image.load()
    image = np.asarray( image, dtype="int32" )
    image_expanded = np.expand_dims(image, axis=0)
    (boxes, scores, classes, num) = sess.run(
        [detection_boxes, detection_scores, detection_classes, num_detections],
        feed_dict={image_tensor: image_expanded})
    a = np.squeeze(classes).astype(np.int32)
    if(list(a).count(1)>list(a).count(2)):
        b = {"result":"1"}
        b = json.dumps(b)
        return HttpResponse(b)
    b = {"result":"2"}
    b = json.dumps(b)
    return HttpResponse(b)
